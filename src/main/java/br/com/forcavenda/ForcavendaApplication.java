package br.com.forcavenda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForcavendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForcavendaApplication.class, args);
	}

}

