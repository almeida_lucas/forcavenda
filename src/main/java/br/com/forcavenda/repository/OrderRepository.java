package br.com.forcavenda.repository;

import br.com.forcavenda.entity.ErrorMessage;
import br.com.forcavenda.entity.OrderHeader;
import br.com.forcavenda.entity.ProductHeader;
import br.com.forcavenda.entity.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class OrderRepository {

  private final EntityManagerFactory entityManagerFactory;

  private final static Logger LOGGER = Logger.getLogger(OrderRepository.class.getName());

  @Autowired
  public OrderRepository(EntityManagerFactory entityManagerFactory) {
    this.entityManagerFactory = entityManagerFactory;
  }

  public void removeOrder(Integer nuNota) throws ErrorMessage {

    EntityManager entityManager = entityManagerFactory.createEntityManager();
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityManager.getTransaction().begin();

    LOGGER.info("removeOrder: " + nuNota);

    StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PKG_APP_FV.DEL_PEDIDOCAB")
        .registerStoredProcedureParameter("P_NUNOTA", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_MSG", String.class, ParameterMode.OUT)
        .setParameter("P_NUNOTA", nuNota);
    query.execute();
    entityManager.flush();

    try {
      if (query.getOutputParameterValue("P_MSG") != null) {
        throw new ErrorMessage("500", "Erro ao deletar pedido " + nuNota + " - " + query.getOutputParameterValue("P_MSG"), null);
      }
    } finally {
      entityTransaction.commit();
      entityManager.close();
    }

  }

  public Integer insertOrderHeader(OrderHeader orderHeader) throws ErrorMessage {

    EntityManager entityManager = entityManagerFactory.createEntityManager();
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityManager.getTransaction().begin();

    LOGGER.info("Date: " + orderHeader.getDtNeg().toString());
    LOGGER.info("String formatted date: " + orderHeader.getFormattedDateDDMMYYYY());
    LOGGER.info("P_CODEMP: " + orderHeader.getCodEmp());
    LOGGER.info("P_CODPARC: " + orderHeader.getCodParc());
    LOGGER.info("P_CODVEND: " + orderHeader.getCodVend());
    LOGGER.info("P_CODTIPOPER: " + orderHeader.getCodTipOper());
    LOGGER.info("P_CODTIPVENDA: " + orderHeader.getCodTipVenda());
    LOGGER.info("P_DTNEG: " + orderHeader.getDtNeg());
    LOGGER.info("P_VLRNOTA: " + orderHeader.getVlrNota());
    LOGGER.info("P_CODNAT: " + orderHeader.getCodNat());
    LOGGER.info("P_OBS: " + orderHeader.getObservacao());
    LOGGER.info("P_STATUSNOTA: " + orderHeader.getStatusNota());
    LOGGER.info("P_NUNOTA: " + 0);
    LOGGER.info("P_AD_NUAPP: " + orderHeader.getNuApp());

    StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PKG_APP_FV.INS_PEDIDOCAB")
        .registerStoredProcedureParameter("P_CODEMP", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_CODPARC", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_CODVEND", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_CODTIPOPER", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_CODTIPVENDA", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_DTNEG", Date.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_VLRNOTA", Float.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_CODNAT", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_OBS", String.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_STATUSNOTA", String.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_NUMNOTA", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_AD_NUAPP", Integer.class, ParameterMode.IN)
        .registerStoredProcedureParameter("P_NUNOTA", Integer.class, ParameterMode.OUT)
        .registerStoredProcedureParameter("P_MSG", String.class, ParameterMode.OUT)
        .setParameter("P_CODEMP", orderHeader.getCodEmp())
        .setParameter("P_CODPARC", orderHeader.getCodParc())
        .setParameter("P_CODVEND", orderHeader.getCodVend())
        .setParameter("P_CODTIPOPER", orderHeader.getCodTipOper())
        .setParameter("P_CODTIPVENDA", orderHeader.getCodTipVenda())
        .setParameter("P_DTNEG", orderHeader.getDtNeg())
        .setParameter("P_VLRNOTA", orderHeader.getVlrNota())
        .setParameter("P_CODNAT", orderHeader.getCodNat())
        .setParameter("P_OBS", orderHeader.getObservacao())
        .setParameter("P_STATUSNOTA", orderHeader.getStatusNota())
        .setParameter("P_NUMNOTA", 0)
        .setParameter("P_AD_NUAPP", orderHeader.getNuApp());
    query.execute();
    entityManager.flush();

    Integer nuNota;

    try {
      if (query.getOutputParameterValue("P_MSG") != null)
        throw new ErrorMessage("500", "Erro ao inserir cabeçalho " + query.getOutputParameterValue("P_MSG"), null);
      else
        nuNota = Integer.valueOf(query.getOutputParameterValue("P_NUNOTA").toString());
    } finally {
      entityTransaction.commit();
      entityManager.close();
    }

    return nuNota;
  }

  public ResponseMessage insertOrderProductList(Integer nuNota, List<ProductHeader> productHeaderList) throws ErrorMessage {
    StringBuilder message = new StringBuilder();

    EntityManager entityManager = entityManagerFactory.createEntityManager();
    EntityTransaction entityTransaction = entityManager.getTransaction();
    entityTransaction.begin();

    List<ProductHeader> productHeaderListToRemove = new ArrayList<>();

    for (ProductHeader productHeader :
        productHeaderList) {
      LOGGER.info("NOVO ITEM");
      LOGGER.info("P_NUNOTA: " + nuNota);
      LOGGER.info("P_CODPROD: " + productHeader.getCod());
      LOGGER.info("P_QTDNEG: " + productHeader.getQtdItens());
      LOGGER.info("P_CODVOL: " + productHeader.getCodVol());
      LOGGER.info("P_CONTROLE: " + productHeader.getLote());
      LOGGER.info("P_VLRUNIT: " + productHeader.getVlrUnit());
      LOGGER.info("P_VLRTOTAL: " + productHeader.getVlrTotal());
      StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PKG_APP_FV.INS_PEDIDOITENS")
          .registerStoredProcedureParameter("P_NUNOTA", Integer.class, ParameterMode.IN)
          .registerStoredProcedureParameter("P_CODPROD", Integer.class, ParameterMode.IN)
          .registerStoredProcedureParameter("P_QTDNEG", Float.class, ParameterMode.IN)
          .registerStoredProcedureParameter("P_CODVOL", String.class, ParameterMode.IN)
          .registerStoredProcedureParameter("P_CONTROLE", String.class, ParameterMode.IN)
          .registerStoredProcedureParameter("P_VLRUNIT", Float.class, ParameterMode.IN)
          .registerStoredProcedureParameter("P_VLRTOTAL", Float.class, ParameterMode.IN)
          .registerStoredProcedureParameter("P_MSG", String.class, ParameterMode.OUT)
          .setParameter("P_NUNOTA", nuNota)
          .setParameter("P_CODPROD", productHeader.getCod())
          .setParameter("P_QTDNEG", productHeader.getQtdItens())
          .setParameter("P_CODVOL", productHeader.getCodVol())
          .setParameter("P_CONTROLE", productHeader.getLote())
          .setParameter("P_VLRUNIT", productHeader.getVlrUnit())
          .setParameter("P_VLRTOTAL", productHeader.getVlrTotal());

      query.execute();

      if (query.getOutputParameterValue("P_MSG") != null) {
        message.append(query.getOutputParameterValue("P_MSG").toString()).append(" - ").append(productHeader.getDescricao());
        productHeaderListToRemove.add(productHeader);
      } else {
        productHeader.setNuNota(nuNota);
      }
    }

    if (!message.toString().isEmpty())
      LOGGER.info("ERRO - P_MSG: " + message);

    productHeaderList.removeAll(productHeaderListToRemove);

    entityManager.flush();
    entityTransaction.commit();
    entityManager.close();

    if (!message.toString().isEmpty()) {
      return new ResponseMessage("500", "Erro ao inserir produtos - " + message, productHeaderList);
    }

    return new ResponseMessage("200", "Pedido " + nuNota + " inserido com sucesso!", productHeaderList);
  }
}
