package br.com.forcavenda.repository;

import br.com.forcavenda.entity.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long>, EntityRepository<Project> {

}
