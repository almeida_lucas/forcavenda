package br.com.forcavenda.repository;

import java.util.List;

public interface EntityRepository<T> {

  List<T> findAll();

}
