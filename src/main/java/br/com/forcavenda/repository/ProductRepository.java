package br.com.forcavenda.repository;

import br.com.forcavenda.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long>, EntityRepository<Product> {

}
