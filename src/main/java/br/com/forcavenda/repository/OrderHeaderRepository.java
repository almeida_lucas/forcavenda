package br.com.forcavenda.repository;

import br.com.forcavenda.entity.OrderHeader;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderHeaderRepository extends CrudRepository<OrderHeader, Long> {

  List<OrderHeader> findByCodVendOrderByNuNota(int codVend);

}
