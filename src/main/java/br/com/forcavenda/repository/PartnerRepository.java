package br.com.forcavenda.repository;

import br.com.forcavenda.entity.Partner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerRepository extends CrudRepository<Partner, Long>, EntityRepository<Partner> {

}
