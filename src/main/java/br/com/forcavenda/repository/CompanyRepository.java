package br.com.forcavenda.repository;

import br.com.forcavenda.entity.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long>, EntityRepository<Company> {

}
