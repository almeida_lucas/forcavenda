package br.com.forcavenda.entity;

import java.io.Serializable;
import java.util.Objects;

public class ProductId implements Serializable {

  private int cod;
  private int lote;

  public ProductId() {
  }

  public ProductId(int cod, int lote) {
    this.cod = cod;
    this.lote = lote;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProductId taskId1 = (ProductId) o;
    if (cod != taskId1.cod) return false;
    return lote == taskId1.lote;
  }

  @Override
  public int hashCode() {
    return Objects.hash(cod, lote);
  }
}
