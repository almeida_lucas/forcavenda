package br.com.forcavenda.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity(name = "VGF_APP_PRODLOTE")
@IdClass(ProductId.class)
public class Product {

  @Id
  @Column(name = "CODPROD")
  private int cod;

  @Id
  @Column(name = "LOTE")
  private int lote;

  @Column(name = "DESCRPROD")
  private String descricao;

  @Column(name = "VOLPAD")
  private String volpad;

  @Column(name = "ESTPAD")
  private String estpad;

  @Column(name = "VLRPAD")
  private String vlrpad;

  @Column(name = "VOLALT")
  private String volalt;

  @Column(name = "ESTALT")
  private String estalt;

  @Column(name = "VLRALT")
  private String vlralt;

  @Column(name = "DESCMAX")
  private String descmax;

  public int getCod() {
    return cod;
  }

  public void setCod(int cod) {
    this.cod = cod;
  }

  public int getLote() {
    return lote;
  }

  public void setLote(int lote) {
    this.lote = lote;
  }

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  public String getVolpad() {
    return volpad;
  }

  public void setVolpad(String volpad) {
    this.volpad = volpad;
  }

  public String getEstpad() {
    return estpad;
  }

  public void setEstpad(String estpad) {
    this.estpad = estpad;
  }

  public String getVlrpad() {
    return vlrpad;
  }

  public void setVlrpad(String vlrpad) {
    this.vlrpad = vlrpad;
  }

  public String getVolalt() {
    return volalt;
  }

  public void setVolalt(String volalt) {
    this.volalt = volalt;
  }

  public String getEstalt() {
    return estalt;
  }

  public void setEstalt(String estalt) {
    this.estalt = estalt;
  }

  public String getVlralt() {
    return vlralt;
  }

  public void setVlralt(String vlralt) {
    this.vlralt = vlralt;
  }

  public String getDescmax() {
    return descmax;
  }

  public void setDescmax(String descmax) {
    this.descmax = descmax;
  }
}
