package br.com.forcavenda.controller;

import br.com.forcavenda.entity.Project;
import br.com.forcavenda.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

  private ProjectRepository projectRepository;

  @Autowired
  ProjectController(ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  @GetMapping("/")
  private List<Project> getProjects() {
    return projectRepository.findAll();
  }

}
