package br.com.forcavenda.controller;

import br.com.forcavenda.entity.ErrorMessage;
import br.com.forcavenda.entity.Order;
import br.com.forcavenda.entity.ResponseMessage;
import br.com.forcavenda.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

  private OrderService orderService;

  @Autowired
  OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  @PostMapping("/insert")
  private ResponseMessage insertOrder(@RequestBody Order order) throws ErrorMessage {
    return orderService.insertOrder(order, false);
//    return new ResponseMessage("200", "Pedido " + newOrder.getHeader().getNuNota() + " inserido com sucesso!", newOrder);
  }

  @PutMapping("/edit")
  private ResponseMessage editOrder(@RequestBody Order order) throws ErrorMessage {
//    int nuNota = order.getHeader().getNuNota();
    return orderService.insertOrder(order, true);
//    return new ResponseMessage("200", "Pedido " + nuNota + " atualizado para " + order.getHeader().getNuNota(), order);
  }

  @DeleteMapping("/delete")
  private ResponseMessage deleteOrder(@RequestHeader int nuNota) throws ErrorMessage {
    orderService.removeOrder(nuNota);
    return new ResponseMessage("200", "Pedido " + nuNota + " removido com sucesso!", nuNota);
  }

  @GetMapping("/orderList")
  private List<Order> getOrderList(@RequestHeader("codVend") int codVend) {
    return orderService.fetchOrderList(codVend);
  }

}
